﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using System.Text;

namespace PhoneBook
{
    class Program
    {
        public static List<Person> People = new List<Person>();
        static void Main(string[] args)
        {
            string file_name = "Notebook.txt";
            Console.Clear();
            Console.WriteLine("Starting.");
            if (System.IO.File.Exists(Convert.ToString(Path.GetFullPath(file_name))) == false)
            {
                Console.WriteLine("Not found Notebook.txt. The file be created automatically. ");
                Console.WriteLine("Ready.");
            }
            Console.ReadKey();
            string command = "";
            while (command != "exit")
            {
                Console.Clear();
                Console.WriteLine("Please enter a command: ");
                command = Console.ReadLine().ToLower();

                switch (command)
                {
                    case "add":
                        AddPerson();
                        break;
                    case "edit":
                        EditPerson();
                        break;
                    case "remove":
                        RemovePerson();
                        break;
                    case "shortlist":
                        ShortListPeople();
                        break;
                    case "list":
                        ListPeople();
                        break;
                    case "search":
                        SearchPerson();
                        break;
                    default:
                        if (command != "exit")
                        {
                            DisplayHelp();
                        }
                        break;
                }
            }
        }

        private static void DisplayHelp()
        {
            Console.Clear();
            Console.WriteLine("Available Commands:");
            Console.WriteLine("add\tAdds a person to book");
            Console.WriteLine("edit\tEditss a person to book");
            Console.WriteLine("remove\tRemoves a person from book");
            Console.WriteLine("list\tLists out all people in the book");
            Console.WriteLine("shortlist\tShot Lists out all people in the book");
            Console.WriteLine("search\tSearches for a person in the book by first name");
            Console.WriteLine("\nPress any key to continue.");
            Console.ReadKey();
        }

        private static void AddPerson()
        {
            Console.Clear();

            Person person = new Person();

            Console.Write("Enter First Name: ");
            bool F = false;
            while (F == false)
            {
                person.FirstName = Console.ReadLine();
                if (!String.IsNullOrEmpty(person.FirstName))
                {
                    F = true;
                }
                else
                {
                    Console.Write("Required. Enter First Name: ");
                }
            }

            Console.Write("Enter Last Name: ");
            bool L = false;
            while (L == false)
            {
                person.LastName = Console.ReadLine();
                if (!String.IsNullOrEmpty(person.LastName))
                {
                    L = true;
                }
                else
                {
                    Console.Write("Required. Enter Last Name: ");
                }
            }

            Console.Write("Enter Patronymic(or click to skip): ");
            person.Patronymic = Console.ReadLine();
            if (String.IsNullOrEmpty(person.Patronymic))
            {
                person.Patronymic = null;
            }

            Console.Write("Enter Phone Number(Only Numbers!): ");
            bool V = false;
            int Number;
            while (V == false)
            {
                string input = Console.ReadLine();
                if (int.TryParse(input, out Number))
                {
                    V = true;
                    person.PhoneNumber = input;
                }
                else
                {
                    Console.WriteLine("Plese, enter numbers only. ");
                }
            }

            Console.Write("Enter Country: ");
            bool C = false;
            while (C == false)
            {
                person.Country = Console.ReadLine();
                if (!String.IsNullOrEmpty(person.Country))
                {
                    C = true;
                }
                else
                {
                    Console.Write("Required. Enter Country: ");
                }
            }

            Console.Write("Enter Date Of Birth(or click to skip): ");
            person.DateOfBirth = Console.ReadLine();
            if (String.IsNullOrEmpty(person.DateOfBirth))
            {
                person.DateOfBirth = null;
            }

            Console.Write("Enter Institution(or click to skip): ");
            person.Institution = Console.ReadLine();
            if (String.IsNullOrEmpty(person.Institution))
            {
                person.Institution = null;
            }


            Console.Write("Enter Post(or click to skip): ");
            person.Post = Console.ReadLine();
            if (String.IsNullOrEmpty(person.Post))
            {
                person.Post = null;
            }

            Console.Write("Enter Other(or enter skip): ");
            person.Other = Console.ReadLine();
            if (String.IsNullOrEmpty(person.Other))
            {
                person.Other = null;
            }

            person.PersonID = Interlocked.Increment(ref Person.globalPersonID);

            People.Add(person);
        }

        private static void RemovePerson()
        {
            List<Person> people = FindPeopleByFirstName();
            Console.Clear();

            if (people.Count == 0)
            {
                Console.WriteLine("That person could not be found. Press any key to continue.");
                Console.ReadKey();
                return;
            }

            foreach (var person in People)
            {
                PrintPerson(person);
            }

            foreach (var person in People)
            {
                if (people.Count == 1)
                {
                    RemovePersonFromList(people.Single());
                    return;
                }
                if (people.Count > 1)
                {
                    Console.WriteLine("Enter ID:");
                    int Number;
                    string input = Console.ReadLine();
                    if (int.TryParse(input, out Number))
                    {
                        person.PersonID = Number;
                        RemovePersonFromList(people.FirstOrDefault());
                        return;
                    }
                }
            }
        }

        private static void RemovePersonFromList(Person person)
        {
            Console.Clear();
            Console.WriteLine("Are you sure you want to remove this person from your book? (Y/N)");
            PrintPerson(person);

            if (Console.ReadKey().Key == ConsoleKey.Y)
            {
                People.Remove(person);
                Console.Clear();
                Console.WriteLine("Person removed. Press any key to continue.");
                Console.ReadKey();
            }
            if (Console.ReadKey().Key == ConsoleKey.N)
            {
                Console.WriteLine("Press any key to continue.");
                Console.ReadKey();
            }
        }

        private static void SearchPerson()
        {
            List<Person> people = FindPeopleByFirstName();
            Console.Clear();
            if (people.Count == 0)
            {
                Console.WriteLine("That person could not be found. Press any key to continue.");
                Console.ReadKey();
                return;
            }

            Console.WriteLine("Here are the current people in your book matching that search:\n");
            foreach (var person in people)
            {
                PrintPerson(person);
            }
            Console.WriteLine("\nPress any key to continue.");
            Console.ReadKey();
        }

        private static List<Person> FindPeopleByFirstName()
        {
            Console.Clear();
            Console.WriteLine("Enter the first name of the person you would like to find.");
            string firstName = Console.ReadLine();
            return People.Where(x => x.FirstName.ToLower() == firstName.ToLower()).ToList();
        }

        private static void ListPeople()
        {
            Console.Clear();
            if (People.Count == 0)
            {
                Console.WriteLine("Your book is empty. Press any key to continue.");
                Console.ReadKey();
                return;
            }
            Console.WriteLine("Here are the current people in your book:\n");
            foreach (var person in People)
            {
                PrintPerson(person);
            }
            Console.WriteLine("\nPress any key to continue.");
            Console.ReadKey();
        }

        private static void ShortListPeople()
        {
            Console.Clear();
            if (People.Count == 0)
            {
                Console.WriteLine("Your book is empty. Press any key to continue.");
                Console.ReadKey();
                return;
            }
            Console.WriteLine("Here are the current people in your book:\n");
            foreach (var person in People)
            {
                PrintPersonShort(person);
            }
            Console.WriteLine("\nPress any key to continue.");
            Console.ReadKey();
        }

        private static void PrintPerson(Person person)
        {
            Console.WriteLine("First Name: " + person.FirstName);
            Console.WriteLine("Last Name: " + person.LastName);
            if (person.Patronymic != null)
            {
                Console.WriteLine("Patronymic: " + person.Patronymic);
            }
            Console.WriteLine("Phone Number: " + person.PhoneNumber);
            Console.WriteLine("Country: " + person.Country);
            if (person.DateOfBirth != null)
            {
                Console.WriteLine("Date Of Birth: " + person.DateOfBirth);
            }
            if (person.Institution != null)
            {
                Console.WriteLine("Institution: " + person.Institution);
            }
            if (person.Post != null)
            {
                Console.WriteLine("Post: " + person.Post);
            }
            if (person.Other != null)
            {
                Console.WriteLine("Other: " + person.Other);
            }
            Console.WriteLine("ID:" + person.PersonID);
            Console.WriteLine("-------------------------------------------");

        }

        private static void PrintPersonShort(Person person)
        {
            Console.WriteLine("First Name: " + person.FirstName);
            Console.WriteLine("Last Name: " + person.LastName);
            Console.WriteLine("Phone Number: " + person.PhoneNumber);
            Console.WriteLine("-------------------------------------------");
        }

        private static void EditPerson()
        {
            List<Person> people = FindPeopleByFirstName();

            Console.Clear();
            if (people.Count == 0)
            {
                Console.WriteLine("That person could not be found. Press any key to continue");
                Console.ReadKey();
                return;
            }
            foreach (var person in People)
            {
                if (people.Count == 1)
                {
                    EdPerson(person);
                }

                if (people.Count > 1)
                {
                    Console.Write("Enter ID: ");
                    int Number;
                    string input = Console.ReadLine();
                    if (int.TryParse(input, out Number))
                    {
                        person.PersonID = Number;
                        EdPerson(person);
                        return;
                    }
                }
            }
        }

        private static void EdPerson(Person person)
        {
            string command = "";
            while (command != "exit")
            {
                Console.Clear();
                Console.WriteLine("Enter command to Edit: ");
                command = Console.ReadLine().ToLower();

                switch (command)
                {
                    case "firstname":
                        Console.WriteLine("Enter new First Name: ");
                        bool F = false;
                        while (F == false)
                        {
                            person.FirstName = Console.ReadLine();
                            if (!String.IsNullOrEmpty(person.FirstName))
                            {
                                F = true;
                            }
                            else
                            {
                                Console.Write("Required. Enter First Name: ");
                            }
                        }
                        break;
                    case "lastname":
                        Console.WriteLine("Enter new Last Name: ");
                        bool L = false;
                        while (L == false)
                        {
                            person.LastName = Console.ReadLine();
                            if (!String.IsNullOrEmpty(person.LastName))
                            {
                                L = true;
                            }
                            else
                            {
                                Console.Write("Required. Enter Last Name: ");
                            }
                        }
                        break;
                    case "patronymic":
                        Console.WriteLine("Enter new Patronymic: ");
                        person.Patronymic = Console.ReadLine();
                        if (String.IsNullOrEmpty(person.Patronymic))
                        {
                            person.Patronymic = null;
                        }
                        break;
                    case "phonenumber":
                        Console.WriteLine("Enter new Phone Number: ");
                        bool V = false;
                        int Number;
                        while (V == false)
                        {

                            string input = Console.ReadLine();
                            if (int.TryParse(input, out Number))
                            {
                                V = true;

                            }
                            else
                            {
                                Console.WriteLine("Plese, enter numbers only.\nPress any key to continue. ");
                                Console.ReadKey();
                            }
                        }
                        break;
                    case "country":
                        Console.WriteLine("Enter new Country: ");
                        bool C = false;
                        while (C == false)
                        {
                            person.Country = Console.ReadLine();
                            if (!String.IsNullOrEmpty(person.Country))
                            {
                                C = true;
                            }
                            else
                            {
                                Console.Write("Required. Enter Country: ");
                            }
                        }
                        break;
                    case "dateofbirth":
                        Console.WriteLine("Enter new Date Of Birth: ");
                        person.DateOfBirth = Console.ReadLine();
                        if (String.IsNullOrEmpty(person.DateOfBirth))
                        {
                            person.DateOfBirth = null;
                        }
                        break;
                    case "institutional":
                        Console.WriteLine("Enter new Institution: ");
                        string c = Console.ReadLine();
                        if (c == "skip")
                        {
                            person.Institution = null;
                        }
                        else
                        {
                            person.Institution = c;
                        }
                        break;
                    case "post":
                        Console.WriteLine("Enter new Post: ");
                        person.Post = Console.ReadLine();
                        if (String.IsNullOrEmpty(person.Post))
                        {
                            person.Post = null;
                        }
                        break;
                    case "other":
                        Console.WriteLine("Enter new Other: ");
                        person.Other = Console.ReadLine();
                        if (String.IsNullOrEmpty(person.Other))
                        {
                            person.Other = null;
                        }
                        break;

                    default:
                        if (command != "exit")
                        {
                            DisplayHelpEdit();
                        }
                        break;
                }
                return;
            }
        }

        private static void DisplayHelpEdit()
        {
            Console.Clear();
            Console.WriteLine("Available Commands:");
            Console.WriteLine("firstname");
            Console.WriteLine("lastname");
            Console.WriteLine("patronymic");
            Console.WriteLine("phonenumber");
            Console.WriteLine("country");
            Console.WriteLine("dateofbirth");
            Console.WriteLine("institution");
            Console.WriteLine("post");
            Console.WriteLine("other");
            Console.WriteLine("\nPress any key to continue.");
            Console.ReadKey();
        }
    }
}

public class Person
{
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Patronymic { get; set; }
    public string PhoneNumber { get; set; }
    public string Country { get; set; }
    public string DateOfBirth { get; set; }
    public string Institution { get; set; }
    public string Post { get; set; }
    public string Other { get; set; }
    public int PersonID { get; set; }

    public static int globalPersonID;


    public Person()
    {
    }
}

